"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_deprecated_1 = require('@angular/router-deprecated');
var shared_1 = require('../../shared');
var landing_1 = require('./landing');
var scenes_1 = require('./scenes');
var suspects_1 = require('./suspects');
var primeng_1 = require('primeng/primeng');
var DashboardComponent = (function () {
    function DashboardComponent(_auth) {
        this._auth = _auth;
        this.mobileMenuActive = false;
    }
    DashboardComponent.prototype.ngOnInit = function () {
        this.loggedIn = this._auth.state();
        if (this.loggedIn) {
            this.authMail = this._auth.getData().password.email;
        }
    };
    DashboardComponent.prototype.toggleMenu = function (e) {
        this.mobileMenuActive = !this.mobileMenuActive;
        e.preventDefault();
    };
    DashboardComponent.prototype.logout = function () {
        this._auth.logout();
        location.reload();
    };
    DashboardComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'app-dashboard',
            templateUrl: 'dashboard.component.html',
            styleUrls: ['dashboard.component.css'],
            directives: [router_deprecated_1.ROUTER_DIRECTIVES, primeng_1.Menu, primeng_1.Button],
            encapsulation: core_1.ViewEncapsulation.None
        }),
        router_deprecated_1.RouteConfig([
            {
                path: '/',
                name: 'Landing',
                component: landing_1.LandingComponent,
                useAsDefault: true
            },
            {
                path: '/scenes/...',
                name: 'Scenes',
                component: scenes_1.ScenesComponent
            },
            {
                path: '/suspects/...',
                name: 'Suspects',
                component: suspects_1.SuspectsComponent
            }
        ]),
        router_deprecated_1.CanActivate(function (next, prev) {
            return shared_1.authCheck(next, prev);
        }), 
        __metadata('design:paramtypes', [shared_1.Auth])
    ], DashboardComponent);
    return DashboardComponent;
}());
exports.DashboardComponent = DashboardComponent;
//# sourceMappingURL=dashboard.component.js.map