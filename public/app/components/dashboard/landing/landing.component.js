"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_deprecated_1 = require('@angular/router-deprecated');
var primeng_1 = require('primeng/primeng');
var scene_service_1 = require('../../../shared/scene.service');
var dossier_service_1 = require('../../../shared/dossier.service');
var LandingComponent = (function () {
    function LandingComponent(_sceneData, _dossierData) {
        this._sceneData = _sceneData;
        this._dossierData = _dossierData;
        this.scenesCnt = [];
        this.dossierCnt = [];
        //_sceneData.scenes.subscribe(r => {this.scenesCnt.push({severity: 'warn', summary: 'Scenes', detail: `${r.length} total`})});
        //_dossierData.dossiers.subscribe(r => this.dossierCnt.push({severity: 'warn', summary: 'Files', detail: `${r.length} total`}));
    }
    LandingComponent.prototype.ngOnInit = function () { };
    LandingComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'app-landing',
            templateUrl: 'landing.component.html',
            providers: [scene_service_1.SceneService, dossier_service_1.DossierService],
            directives: [primeng_1.Messages, primeng_1.InputText, primeng_1.InputTextarea, primeng_1.Button, router_deprecated_1.RouterLink]
        }), 
        __metadata('design:paramtypes', [scene_service_1.SceneService, dossier_service_1.DossierService])
    ], LandingComponent);
    return LandingComponent;
}());
exports.LandingComponent = LandingComponent;
//# sourceMappingURL=landing.component.js.map