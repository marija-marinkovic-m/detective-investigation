"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var common_1 = require('@angular/common');
var router_deprecated_1 = require('@angular/router-deprecated');
var primeng_1 = require('primeng/primeng');
var index_1 = require('../shared/index');
var scene_service_1 = require('./../../../shared/scene.service');
/**
 * LANDING scenes component
 */
var ScenesMain = (function () {
    function ScenesMain(_router, _routeParams, _sceneService) {
        this._router = _router;
        this._routeParams = _routeParams;
        this._sceneService = _sceneService;
    }
    ScenesMain.prototype.ngOnInit = function () {
        this.scenes = this._sceneService.scenes;
        if (this._routeParams.get('id')) {
            this.currentScene = this._routeParams.get('id');
        }
    };
    ScenesMain.prototype.onCreateNew = function () {
        this._router.navigate(['../ScenesNew']);
    };
    ScenesMain.prototype.onEditItem = function ($event) {
        this._router.navigate(['../SingleScene', { id: $event }]);
    };
    ScenesMain.prototype.onDeleteItem = function ($event) {
        if (confirm("Are you sure?")) {
            this._sceneService.deleteOne($event);
        }
    };
    ScenesMain = __decorate([
        core_1.Component({
            template: "\n\t\t<div class=\"ContentSideSections header\">\n\t\t\t<button pButton label=\"Add new scene\" class=\"float-right mt10\" (click)=\"onCreateNew()\"></button>\n\t\t\t<h1>Scenes <img *ngIf=\"!(scenes | async)\" src=\"app/shared/resources/img/alt-loader.gif\" class=\"alt-loader\" alt=\"\" /></h1>\n\t\t</div>\n\t\t<div class=\"ContentSideSections Implementation\">\n\t\t\t<scenes-list \n\t\t\t\t[source]=\"scenes\" \n\t\t\t\t(onEdit)=\"onEditItem($event)\" \n\t\t\t\t(onDelete)=\"onDeleteItem($event)\" \n\t\t\t\t[currentKey]=\"currentScene\"\n\t\t\t></scenes-list>\n\t\t</div>\n\t",
            directives: [index_1.ScenesList, primeng_1.Button]
        }), 
        __metadata('design:paramtypes', [router_deprecated_1.Router, router_deprecated_1.RouteParams, scene_service_1.SceneService])
    ], ScenesMain);
    return ScenesMain;
}());
/**
 * NEW scene form component
 */
var ScenesNew = (function () {
    function ScenesNew(_router, _dataService) {
        this._router = _router;
        this._dataService = _dataService;
        this.msg = [];
        this.loader = false;
    }
    ScenesNew.prototype.onCreate = function ($event) {
        var _this = this;
        this._dataService.add($event).then(function (result) {
            _this.msg.push({ severity: 'info', summary: 'Success', detail: 'Scene successfully created.' });
            _this.loader = true;
            setTimeout(function () {
                _this._router.navigate(['../ScenesMain', { id: result.key() }]);
            }, 1500);
        }, function (reason) {
            _this.msg.push({ severity: 'error', summary: 'Error', detail: reason });
        });
    };
    ScenesNew.prototype.onCancel = function ($event) {
        this._router.navigate(['../ScenesMain']);
    };
    ScenesNew = __decorate([
        core_1.Component({
            template: "\n\t\t<p-growl [value]=\"msg\"></p-growl>\n\t\t<div class=\"ContentSideSections\">\n\t\t\t<h1>New Scene</h1>\n\t\t</div>\n\t\t<div class=\"ContentSideSections Implementation\">\n\t\t\t<scene-form \n\t\t\t\t(onSave)=\"onCreate($event)\" \n\t\t\t\t(onDiscard)=\"onCancel($event)\" \n\t\t\t\t[loading]=\"loader\"\n\t\t\t></scene-form>\n\t\t</div>\n\t",
            styles: [".Implementation {\n\t\tpadding: 30px;\n    \tbackground: white;\n\t}"],
            directives: [index_1.SceneForm, primeng_1.Growl]
        }), 
        __metadata('design:paramtypes', [router_deprecated_1.Router, scene_service_1.SceneService])
    ], ScenesNew);
    return ScenesNew;
}());
/**
 * EDIT scene form component
 */
var SingleScene = (function () {
    function SingleScene(_routeParams, _router, _dataService, _fb) {
        this._routeParams = _routeParams;
        this._router = _router;
        this._dataService = _dataService;
        this._fb = _fb;
        this.msg = [];
        this.loader = false;
        this.sceneId = this._routeParams.get('id');
    }
    SingleScene.prototype.ngOnInit = function () {
        var _this = this;
        this._dataService.getOne(this.sceneId).subscribe(function (res) {
            _this.sceneData = res;
            _this.sceneFormGroup = _this._fb.group({
                title: new common_1.Control(_this.sceneData.title, common_1.Validators.required),
                desc: new common_1.Control(_this.sceneData.desc)
            });
        }, function (err) {
            console.log(err);
        });
    };
    SingleScene.prototype.onUpdateScene = function ($event) {
        var _this = this;
        this._dataService.update(this.sceneId, $event).then(function (res) {
            _this.loader = true;
            _this.msg.push({
                severity: 'info',
                summary: 'Success',
                detail: 'Scene successfuly updated.'
            });
            setTimeout(function () {
                _this._router.navigate(['../ScenesMain', { id: _this.sceneId }]);
            }, 1500);
        }, function (reason) {
            _this.msg.push({
                severity: 'error',
                summary: 'Error',
                detail: reason
            });
        });
    };
    SingleScene.prototype.onCancelEdit = function () {
        this._router.navigate(['../ScenesMain']);
    };
    SingleScene = __decorate([
        core_1.Component({
            moduleId: module.id,
            template: "\n\t\t<p-growl [value]=\"msg\"></p-growl>\n\t\t<div class=\"ContentSideSections\">\n\t\t\t<h1>Edit <strong *ngIf=\"sceneData\">{{ sceneData.title }}</strong></h1>\n\t\t</div>\n\t\t<div class=\"ContentSideSections Implementation\">\n\t\t\t<scene-form \n\t\t\t\t*ngIf=\"sceneFormGroup\" \n\t\t\t\t[formData]=\"sceneFormGroup\" \n\t\t\t\t(onSave)=\"onUpdateScene($event)\" \n\t\t\t\t(onDiscard)=\"onCancelEdit()\" \n\t\t\t\t[loading]=\"loader\"\n\t\t\t></scene-form>\n\t\t</div>\n\t",
            directives: [index_1.SceneForm, primeng_1.Growl],
            styles: [".Implementation {\n\t\tpadding: 30px;\n    \tbackground: white;\n\t}"]
        }), 
        __metadata('design:paramtypes', [router_deprecated_1.RouteParams, router_deprecated_1.Router, scene_service_1.SceneService, common_1.FormBuilder])
    ], SingleScene);
    return SingleScene;
}());
/**
 * SHELL scenes component
 */
var ScenesComponent = (function () {
    function ScenesComponent() {
    }
    ScenesComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'app-scenes',
            template: "<router-outlet></router-outlet>",
            styleUrls: ['scenes.component.css'],
            directives: [router_deprecated_1.RouterOutlet],
            providers: [scene_service_1.SceneService],
            encapsulation: core_1.ViewEncapsulation.None
        }),
        router_deprecated_1.RouteConfig([
            {
                path: '/',
                name: 'ScenesMain',
                component: ScenesMain,
                useAsDefault: true
            },
            {
                path: '/new',
                name: 'ScenesNew',
                component: ScenesNew
            },
            {
                path: '/:id',
                name: 'SingleScene',
                component: SingleScene
            }
        ]), 
        __metadata('design:paramtypes', [])
    ], ScenesComponent);
    return ScenesComponent;
}());
exports.ScenesComponent = ScenesComponent;
//# sourceMappingURL=scenes.component.js.map