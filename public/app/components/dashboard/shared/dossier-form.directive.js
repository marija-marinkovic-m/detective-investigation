"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var common_1 = require('@angular/common');
var primeng_1 = require('primeng/primeng');
var DossierForm = (function () {
    function DossierForm(_fb) {
        this._fb = _fb;
        this.validExtensions = ['.jpg', '.jpeg', '.png'];
        this.maxSize = 524288;
        this.onSave = new core_1.EventEmitter();
        this.onDiscard = new core_1.EventEmitter();
        if (!this.formData) {
            this.buildFormData();
        }
    }
    DossierForm.prototype.buildFormData = function () {
        this.formData = this._fb.group({
            name: new common_1.Control('', common_1.Validators.required),
            gender: new common_1.Control('', common_1.Validators.required),
            profession: new common_1.Control(''),
            age: new common_1.Control(''),
            hair: new common_1.Control(''),
            eyes: new common_1.Control(''),
            figure: new common_1.Control(''),
            tallness: new common_1.Control(''),
            address: new common_1.Control(''),
            police_records: new common_1.Control(''),
            image: new common_1.Control('')
        });
    };
    DossierForm.prototype.onClickSave = function () {
        this.onSave.emit(this.formData.value);
    };
    DossierForm.prototype.onClickCancel = function () {
        this.onDiscard.emit(null);
    };
    DossierForm.prototype.processFile = function (inputEl) {
        var _this = this;
        var reader = new FileReader();
        reader.addEventListener('load', function () {
            // validate type 
            var fileExt = inputEl.value.substring(inputEl.value.lastIndexOf('.')).toLowerCase();
            if (_this.validExtensions.indexOf(fileExt) < 0) {
                alert('Invalid file type');
            }
            else {
                // validate file size 
                var fileSize = inputEl.files[0].size;
                if (fileSize > _this.maxSize) {
                    alert('Maximum file size exceded.');
                }
                else {
                    _this.formData.controls['image'] = _this._fb.control(reader.result);
                    _this.formData.updateValueAndValidity();
                }
            }
        }, false);
        if (inputEl.files[0]) {
            reader.readAsDataURL(inputEl.files[0]);
        }
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Boolean)
    ], DossierForm.prototype, "loading", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', common_1.ControlGroup)
    ], DossierForm.prototype, "formData", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', core_1.EventEmitter)
    ], DossierForm.prototype, "onSave", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', core_1.EventEmitter)
    ], DossierForm.prototype, "onDiscard", void 0);
    DossierForm = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'dossier-form',
            templateUrl: 'dossier-form.directive.html',
            directives: [primeng_1.InputText, primeng_1.Button, primeng_1.InputTextarea, common_1.FORM_DIRECTIVES],
            styles: ["\n\t\th2 .ui-button-icon-only .fa, \n\t\th2 .ui-button-text-icon-left .fa, \n\t\th2 .ui-button-text-icon-right .fa {margin-top: -12px;}\n\t\tform.loading {\n\t\t\tposition: relative; z-index: 1;\n\t\t\t}\n\t\t\tform.loading:before {\n\t\t\t\tcontent: \"\";\n\t\t\t\tposition: absolute; z-index: 9;\n\t\t\t\ttop: 0; left: 0; bottom: 0; right: 0; \n\t\t\t\tbackground: rgba(255,255,255,0.9) url('app/shared/resources/img/loader-trans.gif') center center no-repeat;\n\t\t\t}\n        body {color: #202020;}\n\t"],
            encapsulation: core_1.ViewEncapsulation.None
        }), 
        __metadata('design:paramtypes', [common_1.FormBuilder])
    ], DossierForm);
    return DossierForm;
}());
exports.DossierForm = DossierForm;
//# sourceMappingURL=dossier-form.directive.js.map