"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var primeng_1 = require('primeng/primeng');
var angularfire2_1 = require('angularfire2');
var DossierList = (function () {
    function DossierList() {
        this.curr = '';
        this.onEdit = new core_1.EventEmitter();
        this.onDelete = new core_1.EventEmitter();
        this.checked = false;
        this.baseUrl = window.location.origin;
    }
    DossierList.prototype.onClickTrash = function (key) {
        this.onDelete.emit(key);
    };
    DossierList.prototype.onClickEdit = function (key) {
        this.onEdit.emit(key);
    };
    DossierList.prototype.onClickPrint = function () {
        window.print();
    };
    DossierList.prototype.ngAfterContentChecked = function () {
        if (!this.checked) {
            var holders = document.querySelectorAll('.qr-code-placeholder');
            if (holders.length) {
                this.checked = true;
                for (var i = 0; i < holders.length; i++) {
                    var currLink = this.baseUrl + '/#/profile/' + holders[i].getAttribute('data-key');
                    new QRCode(holders[i], currLink);
                }
            }
        }
    };
    __decorate([
        core_1.Input('source'), 
        __metadata('design:type', angularfire2_1.FirebaseListObservable)
    ], DossierList.prototype, "items", void 0);
    __decorate([
        core_1.Input('currentKey'), 
        __metadata('design:type', String)
    ], DossierList.prototype, "curr", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', core_1.EventEmitter)
    ], DossierList.prototype, "onEdit", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', core_1.EventEmitter)
    ], DossierList.prototype, "onDelete", void 0);
    DossierList = __decorate([
        core_1.Component({
            selector: 'dossier-list',
            template: "\n\t<template [ngIf]=\"items\">\n\t\t<p-tabView orientation=\"left\">\n\t\t\t<p-tabPanel *ngFor=\"let item of items | async\" [header]=\"item.name\" [selected]=\"curr == item.$key\">\n\t\t\t\t<div class=\"toolbar\">\n\t\t\t\t\t<button type=\"button\" class=\"helper-btn\" (click)=\"onClickPrint()\"><i class=\"fa fa-print\"></i></button>\n\t\t\t\t\t<button class=\"helper-btn\" type=\"button\" (click)=\"onClickTrash(item.$key)\"><i class=\"fa fa-trash-o\" aria-hidden=\"true\"></i></button>\n\t\t\t\t\t<button class=\"helper-btn\" type=\"button\" (click)=\"onClickEdit(item.$key)\"><i class=\"fa fa-pencil-square-o\" aria-hidden=\"true\"></i></button>\n\t\t\t\t\t<a href=\"/#/profile/{{item.$key}}\" target=\"_blank\" class=\"helper-btn\"><i class=\"fa fa-external-link\" aria-hidden=\"true\"></i></a>\n\t\t\t\t</div>\n\t\t\t\t<section class=\"ui-grid\">\n\t\t\t\t\t\n\t\t\t\t\t<div class=\"ui-grid-row printable\">\n\t\t\t\t\t\t<div class=\"ui-grid-col-3\">\n\t\t\t\t\t\t\t<h1>{{item.name}}</h1>\n\t\t\t\t\t\t</div>\t\t\t\n\t\t\t\t\t\t<div class=\"ui-grid-col-9 qr-code-placeholder\" [attr.data-key]=\"item.$key\">\n\t\t\t\t\t\t<p style=\"visibility:hidden\">{{ baseUrl + '/profile/' + item.$key }}</p></div>\n\t\t\t\t\t</div>\n\t\t\t\t\t\n\t\t\t\t\t<div class=\"ui-grid-row\" *ngIf=\"item.image\">\n\t\t\t\t\t\t<div class=\"ui-grid-col-3\">Image: </div>\n\t\t\t\t\t\t<div class=\"ui-grid-col-9 profile-image\">\n\t\t\t\t\t\t\t<img [src]=\"item.image\" alt=\"Image\" />\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t\n\t\t\t\t\t<div class=\"ui-grid-row\">\n\t\t\t\t\t\t<div class=\"ui-grid-col-3\">Gender: </div>\n\t\t\t\t\t\t<div class=\"ui-grid-col-9\">{{item.gender}}</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"ui-grid-row\" *ngIf=\"item.profession\">\n\t\t\t\t\t\t<div class=\"ui-grid-col-3\">Profession: </div>\n\t\t\t\t\t\t<div class=\"ui-grid-col-9\">{{item.profession}}</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"ui-grid-row\" *ngIf=\"item.age\">\n\t\t\t\t\t\t<div class=\"ui-grid-col-3\">Age: </div>\n\t\t\t\t\t\t<div class=\"ui-grid-col-9\">{{item.age}}</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"ui-grid-row\" *ngIf=\"item.hair\">\n\t\t\t\t\t\t<div class=\"ui-grid-col-3\">Hair: </div>\n\t\t\t\t\t\t<div class=\"ui-grid-col-9\">{{item.hair}}</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"ui-grid-row\" *ngIf=\"item.eyes\">\n\t\t\t\t\t\t<div class=\"ui-grid-col-3\">Eyes: </div>\n\t\t\t\t\t\t<div class=\"ui-grid-col-9\">{{item.eyes}}</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"ui-grid-row\" *ngIf=\"item.figure\">\n\t\t\t\t\t\t<div class=\"ui-grid-col-3\">Figure: </div>\n\t\t\t\t\t\t<div class=\"ui-grid-col-9\">{{item.figure}}</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"ui-grid-row\" *ngIf=\"item.tallness\">\n\t\t\t\t\t\t<div class=\"ui-grid-col-3\">Tallness: </div>\n\t\t\t\t\t\t<div class=\"ui-grid-col-9\">{{item.tallness}}</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"ui-grid-row\" *ngIf=\"item.address\">\n\t\t\t\t\t\t<div class=\"ui-grid-col-3\">Address: </div>\n\t\t\t\t\t\t<div class=\"ui-grid-col-9\">{{item.address}}</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"ui-grid-row\" *ngIf=\"item.police_records\">\n\t\t\t\t\t\t<div class=\"ui-grid-col-3\">Police records: </div>\n\t\t\t\t\t\t<div class=\"ui-grid-col-9\">{{item.police_records}}</div>\n\t\t\t\t\t</div>\n\t\t\t\t</section>\n\t\t\t</p-tabPanel>\n\t\t</p-tabView>\n\t</template>\n\t",
            directives: [primeng_1.TabView, primeng_1.TabPanel, primeng_1.Button],
            encapsulation: core_1.ViewEncapsulation.Emulated,
            styles: [
                ".profile-image img {\n\t\t\tborder:none;\n\t\t\tmax-width: 150px;\n\t\t\theight: auto;\n\t\t}"
            ]
        }), 
        __metadata('design:paramtypes', [])
    ], DossierList);
    return DossierList;
}());
exports.DossierList = DossierList;
//# sourceMappingURL=dossier-list.directive.js.map