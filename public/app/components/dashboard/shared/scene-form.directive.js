"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var common_1 = require('@angular/common');
var primeng_1 = require('primeng/primeng');
var SceneForm = (function () {
    function SceneForm(_fb) {
        this._fb = _fb;
        this.onSave = new core_1.EventEmitter();
        this.onDiscard = new core_1.EventEmitter();
        if (!this.formData) {
            this.buildFormData();
        }
    }
    SceneForm.prototype.buildFormData = function () {
        this.formData = this._fb.group({
            title: new common_1.Control('', common_1.Validators.required),
            desc: new common_1.Control('')
        });
    };
    SceneForm.prototype.onClickSave = function () {
        this.onSave.emit(this.formData.value);
    };
    SceneForm.prototype.onClickCancel = function () {
        this.onDiscard.emit(null);
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Boolean)
    ], SceneForm.prototype, "loading", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', common_1.ControlGroup)
    ], SceneForm.prototype, "formData", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', core_1.EventEmitter)
    ], SceneForm.prototype, "onSave", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', core_1.EventEmitter)
    ], SceneForm.prototype, "onDiscard", void 0);
    SceneForm = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'scene-form',
            templateUrl: 'scene-form.directive.html',
            directives: [primeng_1.InputText, primeng_1.Header, primeng_1.Editor, primeng_1.Button, common_1.FORM_DIRECTIVES],
            styles: ["\n\t\th2 .ui-button-icon-only .fa, \n\t\th2 .ui-button-text-icon-left .fa, \n\t\th2 .ui-button-text-icon-right .fa {margin-top: -12px;}\n\t\tform.loading {\n\t\t\tposition: relative; z-index: 1;\n\t\t\t}\n\t\t\tform.loading:before {\n\t\t\t\tcontent: \"\";\n\t\t\t\tposition: absolute; z-index: 9;\n\t\t\t\ttop: 0; left: 0; bottom: 0; right: 0; \n\t\t\t\tbackground: rgba(255,255,255,0.9) url('app/shared/resources/img/loader-trans.gif') center center no-repeat;\n\t\t\t}\n\t"],
            encapsulation: core_1.ViewEncapsulation.None
        }), 
        __metadata('design:paramtypes', [common_1.FormBuilder])
    ], SceneForm);
    return SceneForm;
}());
exports.SceneForm = SceneForm;
//# sourceMappingURL=scene-form.directive.js.map