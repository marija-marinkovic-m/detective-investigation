"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var primeng_1 = require('primeng/primeng');
var angularfire2_1 = require('angularfire2');
var ScenesList = (function () {
    function ScenesList() {
        this.curr = '';
        this.onEdit = new core_1.EventEmitter();
        this.onDelete = new core_1.EventEmitter();
    }
    ScenesList.prototype.onClickTrash = function (key) {
        this.onDelete.emit(key);
    };
    ScenesList.prototype.onClickEdit = function (key) {
        this.onEdit.emit(key);
    };
    ScenesList.prototype.onClickPrint = function () {
        window.print();
    };
    __decorate([
        core_1.Input('source'), 
        __metadata('design:type', angularfire2_1.FirebaseListObservable)
    ], ScenesList.prototype, "items", void 0);
    __decorate([
        core_1.Input('currentKey'), 
        __metadata('design:type', String)
    ], ScenesList.prototype, "curr", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', core_1.EventEmitter)
    ], ScenesList.prototype, "onEdit", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', core_1.EventEmitter)
    ], ScenesList.prototype, "onDelete", void 0);
    ScenesList = __decorate([
        core_1.Component({
            selector: 'scenes-list',
            template: "\n\t<template [ngIf]=\"items\">\n\t\t<p-tabView orientation=\"left\">\n\t\t\t<p-tabPanel *ngFor=\"let item of items | async\" [header]=\"item.title\" [selected]=\"curr == item.$key\">\n\t\t\t\t<div class=\"toolbar\">\n\t\t\t\t\t<button type=\"button\" class=\"helper-btn\" (click)=\"onClickPrint()\"><i class=\"fa fa-print\" aria-hidden=\"true\"></i></button>\n\t\t\t\t\t<button class=\"helper-btn\" type=\"button\" (click)=\"onClickTrash(item.$key)\"><i class=\"fa fa-trash-o\" aria-hidden=\"true\"></i></button>\n\t\t\t\t\t<button class=\"helper-btn\" type=\"button\" (click)=\"onClickEdit(item.$key)\"><i class=\"fa fa-pencil-square-o\" aria-hidden=\"true\"></i></button>\n\t\t\t\t</div>\n\t\t\t\t<section [innerHTML]=\"item.desc\"></section>\n\t\t\t</p-tabPanel>\n\t\t</p-tabView>\n\t</template>\n\t",
            directives: [primeng_1.TabView, primeng_1.TabPanel, primeng_1.Button],
            styles: ['.toolbar {margin-bottom: 20px;} .ui-tabview-left > .ui-tabview-nav {height: auto}'],
            encapsulation: core_1.ViewEncapsulation.None
        }), 
        __metadata('design:paramtypes', [])
    ], ScenesList);
    return ScenesList;
}());
exports.ScenesList = ScenesList;
//# sourceMappingURL=scenes-list.directive.js.map