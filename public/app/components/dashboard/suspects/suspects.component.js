"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var common_1 = require('@angular/common');
var router_deprecated_1 = require('@angular/router-deprecated');
var primeng_1 = require('primeng/primeng');
var index_1 = require('../shared/index');
var dossier_service_1 = require('../../../shared/dossier.service');
/**
 * LANDING suspects component
 */
var DossierMain = (function () {
    function DossierMain(_router, _dataService, _params) {
        this._router = _router;
        this._dataService = _dataService;
        this._params = _params;
    }
    DossierMain.prototype.ngOnInit = function () {
        this.dossiers = this._dataService.dossiers;
        this.currRouteParam = this._params.get('id');
    };
    DossierMain.prototype.onCreateNew = function () {
        this._router.navigate(['../DossierNew']);
    };
    DossierMain.prototype.onEditItem = function ($event) {
        this._router.navigate(['../SingleDossier', { id: $event }]);
    };
    DossierMain.prototype.onDeleteItem = function ($event) {
        if (confirm("Are you sure?")) {
            this._dataService.deleteOne($event);
        }
    };
    DossierMain = __decorate([
        core_1.Component({
            template: "\n\t\t<div class=\"ContentSideSections header\">\n\t\t\t<button pButton label=\"Create new file\" class=\"float-right mt10\" (click)=\"onCreateNew()\"></button>\n\t\t\t<h1>Dossiers <img *ngIf=\"!(dossiers | async)\" src=\"app/shared/resources/img/alt-loader.gif\" class=\"alt-loader\" alt=\"\" /></h1>\n\t\t</div>\n\t\t<div class=\"ContentSideSections Implementation\">\n\t\t\t<dossier-list\n\t\t\t\t[source]=\"dossiers\" \n\t\t\t\t(onEdit)=\"onEditItem($event)\" \n\t\t\t\t(onDelete)=\"onDeleteItem($event)\" \n\t\t\t\t[currentKey]=\"currRouteParam\"\n\t\t\t></dossier-list>\n\t\t</div>\n\t",
            directives: [index_1.DossierList, primeng_1.Button]
        }), 
        __metadata('design:paramtypes', [router_deprecated_1.Router, dossier_service_1.DossierService, router_deprecated_1.RouteParams])
    ], DossierMain);
    return DossierMain;
}());
/**
 * NEW dossier form component
 */
var DossierNew = (function () {
    function DossierNew(_router, _dataService) {
        this._router = _router;
        this._dataService = _dataService;
        this.msg = [];
        this.loader = false;
    }
    DossierNew.prototype.onCreate = function ($event) {
        var _this = this;
        this._dataService.add($event).then(function (result) {
            _this.msg.push({ severity: 'info', summary: 'Success', detail: 'File successfully created.' });
            _this.loader = true;
            setTimeout(function () {
                _this._router.navigate(['../DossierMain', { id: result.key() }]);
            }, 1500);
        }, function (reason) {
            _this.msg.push({ severity: 'error', summary: 'Error', detail: reason });
        });
    };
    DossierNew.prototype.onCancel = function ($event) {
        this._router.navigate(['../DossierMain']);
    };
    DossierNew = __decorate([
        core_1.Component({
            template: "\n\t\t<p-growl [value]=\"msg\"></p-growl>\n\t\t<div class=\"ContentSideSections\">\n\t\t\t<h1>New File</h1>\n\t\t</div>\n\t\t<div class=\"ContentSideSections Implementation\">\n\t\t\t<dossier-form \n\t\t\t\t(onSave)=\"onCreate($event)\" \n\t\t\t\t(onDiscard)=\"onCancel($event)\" \n\t\t\t\t[loading]=\"loader\"\n\t\t\t></dossier-form>\n\t\t</div>\n\t",
            styles: [".Implementation {\n\t\tpadding: 30px;\n    \tbackground: white;\n\t}"],
            directives: [index_1.DossierForm, primeng_1.Growl]
        }), 
        __metadata('design:paramtypes', [router_deprecated_1.Router, dossier_service_1.DossierService])
    ], DossierNew);
    return DossierNew;
}());
/**
 * EDIT scene form component
 */
var SingleDossier = (function () {
    function SingleDossier(_routeParams, _router, _dataService, _fb) {
        this._routeParams = _routeParams;
        this._router = _router;
        this._dataService = _dataService;
        this._fb = _fb;
        this.msg = [];
        this.loader = false;
        this.currKey = this._routeParams.get('id');
    }
    SingleDossier.prototype.ngOnInit = function () {
        var _this = this;
        this._dataService.getOne(this.currKey).subscribe(function (res) {
            _this.dossierData = res;
            _this.dossierFormGroup = _this._fb.group({
                name: new common_1.Control(_this.dossierData.name, common_1.Validators.required),
                gender: new common_1.Control(_this.dossierData.gender, common_1.Validators.required),
                profession: new common_1.Control(_this.dossierData.profession),
                age: new common_1.Control(_this.dossierData.age),
                hair: new common_1.Control(_this.dossierData.hair),
                eyes: new common_1.Control(_this.dossierData.eyes),
                figure: new common_1.Control(_this.dossierData.figure),
                tallness: new common_1.Control(_this.dossierData.tallness),
                address: new common_1.Control(_this.dossierData.address),
                image: new common_1.Control(_this.dossierData.image),
                police_records: new common_1.Control(_this.dossierData.police_records)
            });
        }, function (err) {
            console.log(err);
        });
    };
    SingleDossier.prototype.onUpdate = function ($event) {
        var _this = this;
        this._dataService.update(this.currKey, $event).then(function (res) {
            _this.loader = true;
            _this.msg.push({
                severity: 'info',
                summary: 'Success',
                detail: 'File successfuly updated.'
            });
            setTimeout(function () {
                _this._router.navigate(['../DossierMain', { id: _this.currKey }]);
            }, 1500);
        }, function (reason) {
            _this.msg.push({
                severity: 'error',
                summary: 'Error',
                detail: reason
            });
        });
    };
    SingleDossier.prototype.onCancelEdit = function () {
        this._router.navigate(['../DossierMain']);
    };
    SingleDossier = __decorate([
        core_1.Component({
            moduleId: module.id,
            template: "\n\t\t<p-growl [value]=\"msg\"></p-growl>\n\t\t<div class=\"ContentSideSections\">\n\t\t\t<h1>Edit <strong *ngIf=\"dossierData\">{{ dossierData.title }}</strong></h1>\n\t\t</div>\n\t\t<div class=\"ContentSideSections Implementation\">\n\t\t\t<dossier-form \n\t\t\t\t*ngIf=\"dossierFormGroup\" \n\t\t\t\t[formData]=\"dossierFormGroup\" \n\t\t\t\t(onSave)=\"onUpdate($event)\" \n\t\t\t\t(onDiscard)=\"onCancelEdit()\" \n\t\t\t\t[loading]=\"loader\"\n\t\t\t></dossier-form>\n\t\t</div>\n\t",
            styles: [".Implementation {\n\t\tpadding: 30px;\n    \tbackground: white;\n\t}"],
            directives: [index_1.DossierForm, primeng_1.Growl]
        }), 
        __metadata('design:paramtypes', [router_deprecated_1.RouteParams, router_deprecated_1.Router, dossier_service_1.DossierService, common_1.FormBuilder])
    ], SingleDossier);
    return SingleDossier;
}());
/**
 * SHELL suspects component
 */
var SuspectsComponent = (function () {
    function SuspectsComponent() {
    }
    SuspectsComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'app-suspects',
            template: '<router-outlet></router-outlet>',
            styleUrls: ['suspects.component.css'],
            directives: [router_deprecated_1.RouterOutlet],
            providers: [dossier_service_1.DossierService],
            encapsulation: core_1.ViewEncapsulation.None
        }),
        router_deprecated_1.RouteConfig([
            {
                path: '/',
                name: 'DossierMain',
                component: DossierMain,
                useAsDefault: true
            },
            {
                path: '/new',
                name: 'DossierNew',
                component: DossierNew
            },
            {
                path: '/:id',
                name: 'SingleDossier',
                component: SingleDossier
            }
        ]), 
        __metadata('design:paramtypes', [])
    ], SuspectsComponent);
    return SuspectsComponent;
}());
exports.SuspectsComponent = SuspectsComponent;
//# sourceMappingURL=suspects.component.js.map