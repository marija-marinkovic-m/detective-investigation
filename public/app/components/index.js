"use strict";
var dashboard_1 = require('./dashboard');
exports.DashboardComponent = dashboard_1.DashboardComponent;
var login_1 = require('./login');
exports.LoginComponent = login_1.LoginComponent;
var signup_1 = require('./signup');
exports.SignupComponent = signup_1.SignupComponent;
var profile_1 = require('./profile');
exports.ProfileComponent = profile_1.ProfileComponent;
//# sourceMappingURL=index.js.map