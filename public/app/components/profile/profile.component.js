"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_deprecated_1 = require('@angular/router-deprecated');
var dossier_service_1 = require('../../shared/dossier.service');
var index_1 = require('../../shared/index');
var ProfileComponent = (function () {
    function ProfileComponent(_routeParams, _dataService, _renderer) {
        this._routeParams = _routeParams;
        this._dataService = _dataService;
        this._renderer = _renderer;
        this.msgs = [];
        this.search = new core_1.EventEmitter();
    }
    ProfileComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._dataService.getOne(this._routeParams.get('id')).subscribe(function (result) {
            setTimeout(function () {
                _this.profileData = result;
                _this.search.emit(false);
                _this._renderer.setElementClass(document.body, 'bgr-on', true);
            }, 2000);
        }, function (reason) {
            _this.msgs.push({ severity: 'error', summary: 'Error', detail: reason });
            _this.profileData = {
                address: '---',
                age: '---',
                eyes: '---',
                figure: '---',
                gender: '---',
                hair: '---',
                image: '',
                name: '---',
                police_records: '---',
                profession: '---',
                tallness: '---'
            };
            _this.search.emit(false);
        });
    };
    ProfileComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'app-profile',
            templateUrl: 'profile.component.html',
            styleUrls: ['profile.component.css'],
            providers: [dossier_service_1.DossierService],
            pipes: [index_1.RandomizerPipe],
            encapsulation: core_1.ViewEncapsulation.None
        }), 
        __metadata('design:paramtypes', [router_deprecated_1.RouteParams, dossier_service_1.DossierService, core_1.Renderer])
    ], ProfileComponent);
    return ProfileComponent;
}());
exports.ProfileComponent = ProfileComponent;
//# sourceMappingURL=profile.component.js.map