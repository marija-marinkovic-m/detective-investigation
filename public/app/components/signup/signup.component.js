"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_deprecated_1 = require('@angular/router-deprecated');
var shared_1 = require('../../shared');
var SignupComponent = (function () {
    function SignupComponent(_router, _auth) {
        this._router = _router;
        this._auth = _auth;
        this.msg = [];
    }
    SignupComponent.prototype.onLogin = function (event) {
        this._router.navigate(['Login']);
    };
    SignupComponent.prototype.onRegister = function (event) {
        var _this = this;
        this.msg = [];
        this._auth.signup(event).then(function (res) {
            _this._router.navigate(['Dashboard']);
        }, function (err) {
            _this.msg.push({ severity: 'error', summary: 'Register failed', detail: err });
        });
    };
    SignupComponent = __decorate([
        core_1.Component({
            selector: 'app-signup',
            template: "\n\t\t<entry-form [isLogin]=\"false\" (registerSubmit)=\"onRegister($event)\" (loginSubmit)=\"onLogin($event)\" [responseMsg]=\"msg\"></entry-form>\n\t",
            directives: [shared_1.EntryFormDirective]
        }), 
        __metadata('design:paramtypes', [router_deprecated_1.Router, shared_1.Auth])
    ], SignupComponent);
    return SignupComponent;
}());
exports.SignupComponent = SignupComponent;
//# sourceMappingURL=signup.component.js.map