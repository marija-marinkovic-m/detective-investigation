"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_deprecated_1 = require('@angular/router-deprecated');
var index_1 = require('./components/index');
var primeng_1 = require('primeng/primeng');
var CsiProfileAppComponent = (function () {
    function CsiProfileAppComponent() {
    }
    CsiProfileAppComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'csi-profile-app',
            template: '<router-outlet></router-outlet>',
            styleUrls: ['csi-profile.component.css'],
            directives: [router_deprecated_1.ROUTER_DIRECTIVES, primeng_1.Button]
        }),
        router_deprecated_1.RouteConfig([
            {
                path: '/login',
                name: 'Login',
                component: index_1.LoginComponent,
                useAsDefault: true
            },
            {
                path: '/register',
                name: 'Register',
                component: index_1.SignupComponent
            },
            {
                path: '/dashboard/...',
                name: 'Dashboard',
                component: index_1.DashboardComponent
            },
            {
                path: '/profile/:id',
                name: 'Profile',
                component: index_1.ProfileComponent
            }
        ]), 
        __metadata('design:paramtypes', [])
    ], CsiProfileAppComponent);
    return CsiProfileAppComponent;
}());
exports.CsiProfileAppComponent = CsiProfileAppComponent;
//# sourceMappingURL=csi-profile.component.js.map