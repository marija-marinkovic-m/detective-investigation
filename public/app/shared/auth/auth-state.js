"use strict";
var app_injector_1 = require('./app-injector');
var auth_1 = require('./auth');
var router_deprecated_1 = require('@angular/router-deprecated');
exports.authCheck = function (next, prev) {
    var injector = app_injector_1.appInjector();
    var auth = injector.get(auth_1.Auth);
    var router = injector.get(router_deprecated_1.Router);
    return new Promise(function (resolve, reject) {
        if (auth.state()) {
            if (next.urlPath == 'login') {
                router.navigate(['/Dashboard']);
                resolve(false);
            }
            else {
                resolve(true);
            }
        }
        else {
            // user not logged in 
            if (next.urlPath == 'login') {
                resolve(true);
            }
            else {
                router.navigate(['/Login']);
                resolve(false);
            }
        }
    });
};
//# sourceMappingURL=auth-state.js.map