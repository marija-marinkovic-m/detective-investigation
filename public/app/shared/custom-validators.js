"use strict";
var CustomValidators = (function () {
    function CustomValidators() {
    }
    CustomValidators.emailFormat = function (control) {
        var pattern = /\S+@\S+\.\S+/;
        return pattern.test(control.value) ? null : { "emailFormat": true };
    };
    return CustomValidators;
}());
exports.CustomValidators = CustomValidators;
//# sourceMappingURL=custom-validators.js.map