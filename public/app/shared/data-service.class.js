"use strict";
var DataService = (function () {
    function DataService() {
    }
    DataService.prototype.add = function (data) { };
    DataService.prototype.update = function (key, data) { };
    DataService.prototype.deleteOne = function (key) { };
    DataService.prototype.deleteAll = function () { };
    return DataService;
}());
exports.DataService = DataService;
//# sourceMappingURL=data-service.class.js.map