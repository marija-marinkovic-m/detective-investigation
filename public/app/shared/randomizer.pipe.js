"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var Observable_1 = require('rxjs/Observable');
var RandomizerPipe = (function () {
    function RandomizerPipe() {
    }
    RandomizerPipe.prototype.transform = function (type, search, interval) {
        if (type === void 0) { type = 'name'; }
        if (search === void 0) { search = new core_1.EventEmitter(); }
        if (interval === void 0) { interval = 100; }
        var value;
        var data = [];
        switch (type) {
            case 'records':
                data = [
                    'Einen terroristischen Bedrohung',
                    'Bail-Springen in der zweiten Stufe',
                    'Meineid im dritten Grad',
                    'Stalking im zweiten Grad',
                    '',
                    'Rechtswidrige Herstellung von Methamphetamin im zweiten Grad',
                    'Der Besitz von Glücksspiel - Aufzeichnungen',
                    'Straf Verkauf von Marihuana in der zweiten Stufe',
                    'Unlawfully dealing with fireworks and dangerous fireworks',
                    'Manipulieren mit einem Verbraucherprodukt im zweiten Grad',
                    'Straf Besitz einer Waffe in der dritten Grad',
                    'Appearance in public under the influence of narcotics or a drug other than alcohol',
                    ''
                ];
                value = function (data) {
                    var records = '';
                    for (var i = 0; i < 3; i++) {
                        records += ' ' + data[Math.floor(Math.random() * data.length)];
                    }
                    ;
                    return records;
                };
                break;
            case 'address':
                data = [
                    'Genslerstraße 84, Berlin, 13359',
                    'Rudower Strasse 91 Pronsfeld Rheinland-Pfalz 02957',
                    '3899 Elizabeth Street Redford, MI 48239',
                    'Joachimstaler Str. 86 Haide Freistaat Sachsen 37854',
                    '6106 Forest Street Elgin, IL 60120',
                    '3805 Sunset Avenue Dacula, GA 30019',
                    'Gubener Str. 14, Kolbermoor Freistaat Bayern 83053',
                    '5980 King Street Patchogue, NY 11772',
                    '4 Catherine Street Jacksonville, NC 28540'
                ];
                value = function (data) {
                    return data[Math.floor(Math.random() * data.length)];
                };
                break;
            case 'height':
                value = function (data) {
                    return (Math.random() * (2.05 - 0.65) + 1).toFixed(2);
                };
                break;
            case 'figure':
                data = [
                    'Schwer', 'Schlank', 'Skinny', 'Athletic', 'dünn', 'Average', 'Heavy', 'Slim', 'Sportlich', 'durchschnittlich'
                ];
                value = function (data) {
                    return data[Math.floor(Math.random() * data.length)];
                };
                break;
            case 'eyes':
                data = [
                    'Gelb', 'Bernstein', 'Braun', 'Hasel', 'Grün', 'Blau', 'Gray', 'Aqua', 'Purple', 'Pale Brown', 'Deep Blue', 'Hellgrau', 'Emerald Green', 'Meeresgrün'
                ];
                value = function (data) {
                    return data[Math.floor(Math.random() * data.length)];
                };
                break;
            case 'hair':
                data = [
                    'Dunkelblond', 'Black', 'Rothaarige', 'Platinum', 'Dark Redhead', 'Blond', 'Bleichmittel blond', 'Licht Rotschopf', 'Brunette', 'Amber', 'Hazel', 'Hellbraun', 'Blasses Grün', 'Smaragdgrün', 'Violet Red'
                ];
                value = function (data) {
                    return data[Math.floor(Math.random() * data.length)];
                };
                break;
            case 'age':
                value = function (data) {
                    return Math.floor(Math.random() * (56 - 18 + 1)) + 18;
                };
                break;
            case 'profession':
                data = [
                    'Tänzer', 'Computer programmer', 'Estate agent', 'Economist', 'Hairdresser', 'Physician', 'Lawyer', 'Drafter', 'Heating engineer', 'Sozialarbeiter', 'Fitness instructor', 'Bildhauer', 'Matrose', 'Programmierer', 'Chemiker', 'Gas fitter'
                ];
                value = function (data) {
                    return data[Math.floor(Math.random() * data.length)];
                };
                break;
            case 'gender':
                data = [
                    'weiblich', 'männlich', 'male', 'female'
                ];
                value = function (data) {
                    return data[Math.floor(Math.random() * data.length)];
                };
                break;
            default:
                data = [
                    'Eduard Haker', 'Wolfdietrich Kruegel', 'Schubert Hanke', 'Adam Moore', 'Brenda Alexander', 'Harold Peterson', 'Dennis Rogers', 'Amy Phillips', 'Scot Hall', 'Earl Baker', 'Sandra Lee', 'Norma Campbell', 'Daisy Reeves', 'Harriet Jennings', 'Fritz Hoth', 'Willifrank Bierbrauer', 'Morris Nunez', 'Jesus Bishop', 'Alfred Morgan', 'Viola Moore', 'Terri Pena', 'Günter Vechtel', 'Edward Mcgee', 'Ignacio Cohen', 'Ott Schaffner'
                ];
                value = function (data) {
                    return data[Math.floor(Math.random() * data.length)];
                };
        }
        return new Observable_1.Observable(function (subscriber) {
            var currinterval = setInterval(function () {
                subscriber.next(value(data));
            }, interval);
            search.forEach(function (val) {
                if (!val) {
                    clearInterval(currinterval);
                    subscriber.complete();
                }
            });
        });
    };
    RandomizerPipe = __decorate([
        core_1.Pipe({
            name: 'randomize'
        }), 
        __metadata('design:paramtypes', [])
    ], RandomizerPipe);
    return RandomizerPipe;
}());
exports.RandomizerPipe = RandomizerPipe;
//# sourceMappingURL=randomizer.pipe.js.map