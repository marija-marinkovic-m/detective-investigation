"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var angularfire2_1 = require('angularfire2');
var data_service_class_1 = require('./data-service.class');
var SceneService = (function (_super) {
    __extends(SceneService, _super);
    function SceneService(_af) {
        _super.call(this);
        this._af = _af;
        this.scenes = this._af.database.list('/services');
    }
    SceneService.prototype.add = function (data) {
        return this.scenes.push(data);
    };
    SceneService.prototype.update = function (key, data) {
        return this.scenes.update(key, data);
    };
    SceneService.prototype.deleteOne = function (key) {
        return this.scenes.remove(key);
    };
    SceneService.prototype.deleteAll = function () {
        return this.scenes.remove();
    };
    SceneService.prototype.getOne = function (key) {
        return this._af.database.object('/services/' + key);
    };
    SceneService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [angularfire2_1.AngularFire])
    ], SceneService);
    return SceneService;
}(data_service_class_1.DataService));
exports.SceneService = SceneService;
//# sourceMappingURL=scene.service.js.map