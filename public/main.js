"use strict";
var platform_browser_dynamic_1 = require('@angular/platform-browser-dynamic');
var core_1 = require('@angular/core');
var common_1 = require('@angular/common');
var _1 = require('./app/');
var angularfire2_1 = require('angularfire2');
var shared_1 = require('./app/shared');
var router_deprecated_1 = require('@angular/router-deprecated');
if (_1.environment.production) {
    core_1.enableProdMode();
}
platform_browser_dynamic_1.bootstrap(_1.CsiProfileAppComponent, [
    router_deprecated_1.ROUTER_PROVIDERS,
    core_1.provide(common_1.LocationStrategy, { useClass: common_1.HashLocationStrategy }),
    angularfire2_1.FIREBASE_PROVIDERS,
    angularfire2_1.defaultFirebase('https://csi-profile-app.firebaseio.com/'),
    angularfire2_1.firebaseAuthConfig({
        provider: angularfire2_1.AuthProviders.Password,
        method: angularfire2_1.AuthMethods.Password
    }),
    shared_1.Auth
]).then(function (appRef) {
    shared_1.appInjector(appRef.injector);
}, function (err) { return console.log(err); });
//# sourceMappingURL=main.js.map